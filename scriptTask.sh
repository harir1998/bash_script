#!/bin/bash

function checkPackageManager(){
    if [ $(which apt) ]
    then
        packageManager=apt
    else
        packageManager=yum
    fi
}

function installPackages(){
    sudo $packageManager update
    
    case $packageManager in
        yum)
            for package in httpd mysql-server php git curl php-mysqli
            do
                if ! [ $(which $package) ]
                then
                    sudo yum install -y $package
                else
                    echo $package already installed
                fi
            done
                ;;
        apt)
            for package in apache2 mysql-server php git curl php-mysqli
            do
                if ! [ $(which $package) ]
                then
                    sudo apt install -y $package
                else
                    echo $package already installed
                fi
            done
                ;;
    esac
}

function checkAndStartService(){
    if [ $(sudo systemctl is-active $1 ) = inactive ]
        then
        sudo systemctl start $1
        echo $1 has started
    else
        echo $1 is up already
    fi
}

function stopService(){
    sudo systemctl stop $1
    echo $1 has stopped
}

function showStatus(){
    echo Mysql is $(sudo systemctl is-active mysql)
    mysql -V
    if [ $packageManager = apt ]
            then
                echo Apache2 is $(sudo systemctl is-active apache2)
                apache2 -v
            else
                echo httpd is $(sudo systemctl is-active httpd)
                httpd -v
            fi
    php -v
}

function configureDB(){
    cat > configure-DB.sql <<EOF
    CREATE USER IF NOT EXISTS 'mysql_user'@'localhost' IDENTIFIED BY 'mysql_password';
    GRANT ALL PRIVILEGES ON *.* TO 'mysql_user'@'localhost';
    FLUSH PRIVILEGES;
EOF
    sudo mysql < configure-DB.sql
    sudo rm configure-DB.sql
}

function cronjobBackup(){
    cat > backupDB.sh <<EOF
    sudo /usr/bin/mysqldump -u mysql_user -pmysql_password mysql > backupDB.sql
    sudo mkdir -p /opt/backups
    sudo mv backupDB.sql "/opt/backups/$(date +%F).sql"
EOF

chmod +x backupDB.sh
sudo mv ./backupDB.sh /home/backupDB.sh

echo "0 0 * * * sudo /home/backupDB.sh" >> crontab_new
sudo crontab crontab_new -u root

rm crontab_new

}


checkPackageManager

while getopts ":hisxyctab" option; do
   case $option in
        h) # Help -h
            echo Help
            echo -i     Install packages
            echo -s     Start Services
            echo -x     Stop Services
            echo -c     Clone PHP app from github
            echo -y     Show Status
            echo -a     Add user in mysql
            echo -t     Curl to application
            echo -b     Setup cronjob
            ;;
        i) # install -i
            echo install
            installPackages
            ;;
        s) # install -s
            echo start
            checkAndStartService apache2
            checkAndStartService mysql
            ;;
        x) # stop -x
            echo stop
            stopService mysql
            if [ $packageManager = apt ]
            then
                stopService apache2
            else
                stopService httpd
            fi
            ;;
        y) # status -y
            echo status
            showStatus
            ;;
        c) # clone app -c
            sudo rm -rf /var/www/html/simplePHP
            git clone https://harir1998@bitbucket.org/harir1998/simple_php.git /var/www/html/simplePHP/

            ;;
        t) # curl hosted application
            curl localhost/simplePHP/index.php
            ;;
        b) # setup backup script and place the cronjob
            cronjobBackup
            ;;
        a) #add mysql user
            configureDB
            ;;
        *) echo run with -h for help ;;
   esac
done